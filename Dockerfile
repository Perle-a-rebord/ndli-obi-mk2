# Use the official Node.js image as parent image
FROM node:19.2-slim

# Set the working directory. If it doesn't exists, it'll be created
WORKDIR /app

ENV PATH /usr/src/app/node_modules/.bin:$PATH
# Define the env variable 
ENV PORT 3000

# Expose the port 3000
EXPOSE ${PORT} 

# Copy the file  from current folder
# inside our image in the folder 
COPY ./back-end/package.json /app/package.json

# Install the dependencies
RUN npm install
COPY back-end/. /app
# Start the app
ENTRYPOINT ["npm", "start"]
