#REQUETES SQL A FAIRE 

#TABLE JOUEUR

SELECT * FROM t_joueur_jou;
SELECT jou_pseudo FROM t_joueur_jou WHERE jou_id=1;
SELECT jou_scorePVZ, jou_scoreTCG, jou_scoreRPG FROM t_joueur_jou WHERE jou_id=1;
SELECT jou_nbCanettes FROM t_joueur_jou WHERE jou_id=1;

UPDATE t_joueur_jou SET jou_scorePVZ=3000 WHERE jou_id=1;
UPDATE t_joueur_jou SET jou_scoreTCG=3000 WHERE jou_id=1;
UPDATE t_joueur_jou SET jou_scoreRPG=3000 WHERE jou_id=1;
UPDATE t_joueur_jou SET jou_nbCanettes=1 WHERE jou_id=1;

INSERT INTO t_joueur_jou VALUES (NULL, 'pseudodujoueur', 0, 0, 0, 0);

DELETE FROM t_joueur_jou WHERE jou_id=1;

DELIMITER //
CREATE FUNCTION scoreGlobal(id INT) RETURNS INT
BEGIN
	SET @a := (SELECT jou_scorePVZ FROM t_joueur_jou WHERE jou_id=id);
	SET @a := @a + (SELECT jou_scoreRPG FROM t_joueur_jou WHERE jou_id=id);
	SET @a := @a + (SELECT jou_scoreTCG FROM t_joueur_jou WHERE jou_id=id);
RETURN @a;
END;
//DELIMITER ;
SELECT scoreGlobal(1);

#TABLE JEU

SELECT * FROM t_jeu_jeu;
SELECT jeu_nom FROM t_jeu_jeu WHERE jeu_id=0;
SELECT jeu_description FROM t_jeu_jeu WHERE jeu_id=0;
SELECT jeu_cheminImage FROM t_jeu_jeu WHERE jeu_id=0;

UPDATE t_jeu_jeu SET jeu_description='Description du jeu' WHERE jeu_id=1;
UPDATE t_jeu_jeu SET jeu_nom='FinalFantasy' WHERE jeu_id=1;
UPDATE t_jeu_jeu SET jeu_cheminImage='../images/jeuRPG.jpg' WHERE jeu_id=1;

INSERT INTO t_jeu_jeu VALUES (NULL, 'nomdujeu', 'descriptiondujeu', 'chemindaccesalimagedujeu');

DELETE FROM t_jeu_jeu WHERE jeu_id=1;


#TABLE IST

SELECT * FROM t_ist_ist;
SELECT ist_nom FROM t_ist_ist WHERE ist_id=1;
SELECT ist_description FROM t_ist_ist WHERE ist_id=1;
SELECT ist_cheminImage FROM t_ist_ist WHERE ist_id=1;

UPDATE t_ist_ist SET ist_description='fait bobo très fort' WHERE ist_id=1;
UPDATE t_ist_ist SET ist_nom='istnumero1' WHERE ist_id=1;
UPDATE t_ist_ist SET ist_cheminImage='index/IST/numero1' WHERE ist_id=1;

INSERT INTO t_ist_ist VALUES (NULL,'Chlamydiae','Ist sacrément méchante','index/IST/id_chlamydiae');

DELETE FROM t_ist_ist WHERE ist_id=1;

#TABLE PROTECTION

SELECT * FROM t_protection_pro;
SELECT pro_nom FROM t_protection_pro WHERE pro_id=1;
SELECT pro_description FROM t_protection_pro WHERE pro_id=1;
SELECT pro_cheminImage FROM t_protection_pro WHERE pro_id=1;

UPDATE t_protection_pro SET pro_description='fait bobo très fort' WHERE pro_id=1;
UPDATE t_protection_pro SET pro_nom='istnumero1' WHERE pro_id=1;
UPDATE t_protection_pro SET ipro_cheminImage='index/IST/numero1' WHERE pro_id=1;

INSERT INTO t_protection_pro VALUES (NULL,'Préservatif','Plein de couleur et marque certain rembourser par la secu','index/PROTECTION/id_prÃ©servatif');

DELETE FROM t_protection_pro WHERE pro_id=1;

#TABLE MEDICAMENT

SELECT * FROM t_medicament_med;
SELECT med_nom FROM t_medicament_med WHERE med_id=1;
SELECT med_description FROM t_medicament_med WHERE med_id=1;
SELECT med_cheminImage FROM t_medicament_med WHERE med_id=1;

UPDATE t_medicament_med SET med_description='fait bobo très fort' WHERE pro_id=1;
UPDATE t_medicament_med SET med_nom='istnumero1' WHERE pro_id=1;
UPDATE t_medicament_med SET med_cheminImage='index/IST/numero1' WHERE pro_id=1;


INSERT INTO t_medicament_med VALUES (NULL,'TPE','Pillule après cotamination rÃ©duit les riques','index/MEDOC/id_tpe');

DELETE FROM t_medicament_med WHERE med_id=1;

#TABLE PARTIE

SELECT * FROM t_partie_par;
SELECT jeu_id FROM t_partie_par WHERE par_id=1;
SELECT jou_id FROM t_partie_par WHERE par_id=1;

UPDATE t_partie_par SET par_description='fait bobo très fort' WHERE par_id=1;
UPDATE t_partie_par SET par_nom='istnumero1' WHERE par_id=1;
UPDATE t_partie_par SET par_cheminImage='index/IST/numero1' WHERE par_id=1;

DELETE FROM t_partie_par ; #attention supprime toutes les lignes de partie

INSERT INTO t_partie_par VALUES (NULL,1,4);

DELETE FROM t_partie_par WHERE par_id=1;
