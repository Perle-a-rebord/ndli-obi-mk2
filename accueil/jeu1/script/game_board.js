let cardIndex = 0;
let pvPlayer = 50;
let pvBoss = 50;
let pointsAttackBooss = 0;
let activatedDefender = 0;
let boost = 1;
let cptPoison = 0;
let test = false;


function game() {
    cardIndex = 0;
    displayCard();
    actualizePvPlayer();
    actualizePvBoss();
}

let cards = document.getElementsByClassName("carte");
playerCards = [];

function entierAleatoire(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
for (let i = 0; i < 5; i++) {
    playerCards.push(Math.floor(Math.random() * 16));
}

function nextPrevCard(n) {
    cardIndex = cardIndex + n;
    displayCard();
}

function displayCard() {
    if (cardIndex > 4) {
        cardIndex = 0;
    }
    if (cardIndex < 0) {
        cardIndex = 4;
    }
    for (let i = 0; i < 16; i++) {
        cards[i].style.display = "none";
    }
    cards[playerCards[cardIndex]].style.display = "flex";
}

function pioche() {
    playerCards[cardIndex] = Math.floor(Math.random() * 16);
    displayCard();

}

function actualizePvBoss() {
    let divpvboss = document.getElementById("pv_boss");
    divpvboss.innerHTML = pvBoss;
}

function actualizePvPlayer() {
    let divpvplayer = document.getElementById("pv_joueur");
    divpvplayer.innerHTML = pvPlayer + "+" + activatedDefender;


}

function bossAttack() {

    var nb = Math.floor(Math.random() * (5 - 2 + 1)) + 2;

    if (activatedDefender > 0) {
        if (activatedDefender > nb) {
            activatedDefender -= nb;
        }
        else {
            nb -= activatedDefender;
            activatedDefender = 0;
            pvPlayer -= nb;
        }
    }
    else {
        pvPlayer -= nb;
    }

    actualizePvPlayer();
}




function card0() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    activatedDefender += 7 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card1() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    activatedDefender += 4 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card2() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    activatedDefender += 3 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card3() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    activatedDefender += 2 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card4() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    activatedDefender += 6 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card5() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvBoss -= 3 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card6() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvBoss -= 7 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card7() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvBoss -= 4 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card8() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    var nb = Math.floor(Math.random() * (6 - 2 + 1)) + 2;
    pvBoss -= nb * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card9() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvBoss -= 3 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card10() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvBoss -= 4 * boost;
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card11() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvBoss -= 1;
    if (test) {
        pvBoss -= 3 * boost;
    }
    boost = 1;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card12() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    test = false;
    actualizePvBoss();
    pioche();
}

function card13() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    boost = 2;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card14() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    pvPlayer += 8;
    test = false;
    bossAttack();
    actualizePvBoss();
    pioche();
}

function card15() {
    if (cptPoison > 0) {
        pvBoss -= 1;
        cptPoison -= 1;
    }
    test = false;
    cptPoison = 3;
    bossAttack();
    actualizePvBoss();
    pioche();
}

