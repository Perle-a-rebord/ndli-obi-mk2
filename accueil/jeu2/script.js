let fps = 10;
let timerID = setInterval(update, 1000/fps);

let protections;
let medocs1;
let cachets;
let capotes;
let medocs2;
let gelules;
let ists;
let contact;
let money;

let xMousePos = 0;
let yMousePos = 0;

let gameDiv;
let spawn;

let istHordeTxt = "";
let istHordeObj = [];

let cachetObj = [];

let listMedocs1 = [];
let listMedocs2 = [];
let listCapotes = [];

let protecAchetee;


// CLASSES

class ist {
	constructor(vie, attaque, vitesse, ligne) {
		this.vie = vie;
		this.attaque = attaque;
		this.vitesse = vitesse;
		this.ligne = ligne;
	}

	get getVie() {
		return this.vie;
	}
	set setVie(newVie) {
		this.vie = newVie;
	}

	get getAttaque() {
		return this.attaque;
	}
	set setAttaque(newAttaque) {
		this.attaque = newAttaque;
	}

	get getLigne() {
		return this.ligne;
	}
	set setLigne(newLigne) {
		this.ligne = newLigne;
	}
}

class pillule {
	constructor(ligne, degats) {
		this.ligne = ligne;
		this.degats = degats;
	}

	get ligne() {
		return this.ligne;
	}

	get x() {
		return this.x;
	}

	get degats() {
		return this.degats;
	}
}

class medoc1 { //canon
	constructor(caseRes) {
		this.caseRes = caseRes;
		this.attaque = 10;
		this.vie = 5;
		this.charge = 0;
	}

	tir() {
		if (this.charge > 100) {
			this.charge = 0;
			cachetObj.push(new cachet(1));
		} else {
			this.charge += 100 / fps;
		}
	}
}

class cachet extends pillule {
	constructor(ligne) {
		this.ligne = ligne;
		this.vitesse = 10; //A r�gler
	}

	move() {
		this.x += this.vitesse;
	}
}

class capote {
	constructor(caseRes) {
		this.caseRes = caseRes;
		this.vie = 50;
	}
}

class medoc2 { //mines
	constructor(caseRes) {
		this.caseRes = caseRes;
		this.attaque = 20;
		this.vie = 10;
		this.charge = 0;
	}

	place_gelule() {
		if (this.charge > 100) {
			this.charge = 0;
			//place gelule
		} else {
			this.charge += 100 / fps / 2; //tir toutes les 2s
		}
	}
}

class gelule extends pillule {
	constructor(colonne) {
		this.colonne = colonne;
	}
}


// FUNCTION

function init() {
	gameDiv = document.getElementById("game");
	spawn = document.getElementById("spawn");


	gameDiv.onmousemove = function (e) {
		xMousePos = e.clientX + window.pageXOffset;
		yMousePos = e.clientY + window.pageYOffset;
	}

	spawnIST();
}

function update()
{
	ists = document.getElementsByClassName("ist");
	protections = document.getElementsByClassName("protection");
	pillules = document.getElementsByClassName("pillule");
	medocs1 = document.getElementsByClassName("medoc1");
	cachets = document.getElementsByClassName("cachet");
	capotes = document.getElementsByClassName("capotes");
	medocs2 = document.getElementsByClassName("medoc2");
	gelules = document.getElementsByClassName("gelule");
	let i = 0;
	let j = 0;
	for (i = 0; i < ists.length; i++) {
		contact = false;
		for (j = 0; j < protections.length; j++) {
			if (istHordeObj[i].ligne==protections[i].ligne) {
				if (istHordeObj[i].x < protections[i].colonne/*x taille case*/) {
					contact = true;
					protection[i].vie -= istHordeObj[i].attaque;
				}
			}
		}
		if (!contact) {
			let istObj = ists[i];
			let marginRight = parseInt(window.getComputedStyle(istObj).marginRight);
			marginRight += istHordeObj[i].vitesse;
			istObj.style.marginRight = marginRight + "px";
		}
	}

	for (i = 0; i < medocs1.length; i++) {
		medocs1[i].tir();
	}

	for (i = 0; i < medocs2.length; i++) {
		medocs2[i].place_mine();
	}

	for (i = 0; i < cachets.length; i++) {
		cachets[i].move();
	}

	for (i = 0; i < pillules.length; i++) {
		contact = false;
		for (j = 0; j < ists.length; j++) {
			if (pillules[i].ligne == istHordeObj[i].ligne) {
				if (pillules[i].x < istHordeObj[i].x+5 && pillules[i].x > istHordeObj[i].x-5) {
					contact = true;
					istHordeObj[i].setVie = stHordeObj[i].setVie - pillules[i].degats;
					//supprimer pillule
					//if ist.vie=0 supprimer ist puis argent augmente
				}
			}
		}
	}



	//if(zombie face � plante){inflige_degat_a(zombie,plante} else {zombie avance}
	//if(zombie sur mine){mine explose}
	//if(plante charg�e){plante.tir()} else {plante charge}
	//pour toutes les balles: balles avancent
	//argent augmente
}

function achete(id)
{
	protecAchetee = id;
}

function pose(id)
{
	let caseTerrain = document.getElementById(id);
	let txt = "<img src=\"../img/";
	if (protecAchetee==0) {
		txt += "prep_medoc";
		listMedocs1.push(new medoc1(id));
	} else if (protecAchetee==1) {
		txt += "capote";
		listCapotes.push(new capote(id));
	} else {
		txt += "medoc_tasp";
		listMedocs2.push(new medoc2(id));
	}
	txt += ".png\" height=\"100%\">"
	caseTerrain.innerHTML = txt;
}

function spawnIST()
{
	istHordeTxt += "<div class=\"ist\" style=\"position:absolute; right:0\"><img src=\"../img/virus2.png\"></div>";
	spawn.innerHTML = istHordeTxt;
	istHordeObj.push(new ist(20, 5, 10, 1));
}